import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job

## @params: [JOB_NAME]
args = getResolvedOptions(sys.argv, ['JOB_NAME'])

sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)
## @type: DataSource
## @args: [database = "challenge-db", table_name = "mx_product_dim", transformation_ctx = "DataSource0"]
## @return: DataSource0
## @inputs: []
DataSource0 = glueContext.create_dynamic_frame.from_catalog(database = "challenge-db", table_name = "mx_product_dim", transformation_ctx = "DataSource0")
## @type: ApplyMapping
## @args: [mappings = [("col0", "long", "id_product", "long"), ("col1", "string", "calorie_category", "string"), ("col2", "string", "product", "string"), ("col3", "string", "product_brand", "string"), ("col4", "string", "producer", "string")], transformation_ctx = "Transform2"]
## @return: Transform2
## @inputs: [frame = DataSource0]
Transform2 = ApplyMapping.apply(frame = DataSource0, mappings = [("col0", "long", "id_product", "long"), ("col1", "string", "calorie_category", "string"), ("col2", "string", "product", "string"), ("col3", "string", "product_brand", "string"), ("col4", "string", "producer", "string")], transformation_ctx = "Transform2")
## @type: DataSink
## @args: [connection_type = "s3", catalog_database_name = "challenge-db", format = "json", connection_options = {"path": "s3://aws-bucket-challenge/crudo/inventada_sa_de_cv/POS/", "partitionKeys": [], "enableUpdateCatalog":true, "updateBehavior":"UPDATE_IN_DATABASE"}, catalog_table_name = "catalog-raw-products", transformation_ctx = "DataSink2"]
## @return: DataSink2
## @inputs: [frame = Transform2]
DataSink2 = glueContext.getSink(path = "s3://aws-bucket-challenge/crudo/inventada_sa_de_cv/POS/", connection_type = "s3", updateBehavior = "UPDATE_IN_DATABASE", partitionKeys = [], enableUpdateCatalog = True, transformation_ctx = "DataSink2")
DataSink2.setCatalogInfo(catalogDatabase = "challenge-db",catalogTableName = "catalog-raw-products")
DataSink2.setFormat("json")
DataSink2.writeFrame(Transform2)

## @type: DataSource
## @args: [database = "challenge-db", table_name = "mx_tamales_inc", transformation_ctx = "DataSource2"]
## @return: DataSource2
## @inputs: []
DataSource2 = glueContext.create_dynamic_frame.from_catalog(database = "challenge-db", table_name = "mx_tamales_inc", transformation_ctx = "DataSource2")
## @type: ApplyMapping
## @args: [mappings = [("col0", "long", "year", "long"), ("col1", "string", "month", "string"), ("col2", "string", "country", "string"), ("col3", "string", "calorie_category", "string"), ("col4", "string", "flavor", "string"), ("col5", "string", "zone", "string"), ("col6", "string", "product_code", "string"), ("col7", "string", "product_name", "string"), ("col8", "string", "sales", "string")], transformation_ctx = "Transform1"]
## @return: Transform1
## @inputs: [frame = DataSource2]
Transform1 = ApplyMapping.apply(frame = DataSource2, mappings = [("col0", "long", "year", "long"), ("col1", "string", "month", "string"), ("col2", "string", "country", "string"), ("col3", "string", "calorie_category", "string"), ("col4", "string", "flavor", "string"), ("col5", "string", "zone", "string"), ("col6", "string", "product_code", "string"), ("col7", "string", "product_name", "string"), ("col8", "string", "sales", "string")], transformation_ctx = "Transform1")
## @type: DataSink
## @args: [connection_type = "s3", catalog_database_name = "challenge-db", format = "json", connection_options = {"path": "s3://aws-bucket-challenge/crudo/tamal-inc/erp/", "partitionKeys": ["year" ,"month"], "enableUpdateCatalog":true, "updateBehavior":"UPDATE_IN_DATABASE"}, catalog_table_name = "catalog-raw-ventas", transformation_ctx = "DataSink0"]
## @return: DataSink0
## @inputs: [frame = Transform1]
DataSink0 = glueContext.getSink(path = "s3://aws-bucket-challenge/crudo/tamal-inc/erp/", connection_type = "s3", updateBehavior = "UPDATE_IN_DATABASE", partitionKeys = ["year","month"], enableUpdateCatalog = True, transformation_ctx = "DataSink0")
DataSink0.setCatalogInfo(catalogDatabase = "challenge-db",catalogTableName = "catalog-raw-ventas")
DataSink0.setFormat("json")
DataSink0.writeFrame(Transform1)

## @type: DataSource
## @args: [database = "challenge-db", table_name = "mx_fact_table", transformation_ctx = "DataSource1"]
## @return: DataSource1
## @inputs: []
DataSource1 = glueContext.create_dynamic_frame.from_catalog(database = "challenge-db", table_name = "mx_fact_table", transformation_ctx = "DataSource1")
## @type: ApplyMapping
## @args: [mappings = [("col0", "long", "year", "int"), ("col1", "string", "month", "string"), ("col2", "string", "sales", "double"), ("col3", "long", "id_region", "long"), ("col4", "long", "id_product", "long")], transformation_ctx = "Transform0"]
## @return: Transform0
## @inputs: [frame = DataSource1]
Transform0 = ApplyMapping.apply(frame = DataSource1, mappings = [("col0", "long", "year", "int"), ("col1", "string", "month", "string"), ("col2", "string", "sales", "double"), ("col3", "long", "id_region", "long"), ("col4", "long", "id_product", "long")], transformation_ctx = "Transform0")
## @type: DataSink
## @args: [connection_type = "s3", catalog_database_name = "challenge-db", format = "json", connection_options = {"path": "s3://aws-bucket-challenge/crudo/inventada_sa_de_cv/POS/", "partitionKeys": ["year" ,"month"], "enableUpdateCatalog":true, "updateBehavior":"UPDATE_IN_DATABASE"}, catalog_table_name = "catalog-raw-fact", transformation_ctx = "DataSink1"]
## @return: DataSink1
## @inputs: [frame = Transform0]
DataSink1 = glueContext.getSink(path = "s3://aws-bucket-challenge/crudo/inventada_sa_de_cv/POS/", connection_type = "s3", updateBehavior = "UPDATE_IN_DATABASE", partitionKeys = ["year","month"], enableUpdateCatalog = True, transformation_ctx = "DataSink1")
DataSink1.setCatalogInfo(catalogDatabase = "challenge-db",catalogTableName = "catalog-raw-fact")
DataSink1.setFormat("json")
DataSink1.writeFrame(Transform0)

job.commit()