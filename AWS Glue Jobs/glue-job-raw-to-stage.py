import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue.dynamicframe import DynamicFrameCollection
from awsglue.dynamicframe import DynamicFrame

def FilterNegatives(glueContext, dfc) -> DynamicFrameCollection:
    df = dfc.select(list(dfc.keys())[0]).toDF()
    df_filtered = df.filter(df["sales"] >= 0)
    dyf_filtered = DynamicFrame.fromDF(df_filtered, glueContext, "sales")
    return(DynamicFrameCollection({"CustomTransform0": dyf_filtered}, glueContext))
def MyTransform2(glueContext, dfc) -> DynamicFrameCollection:
    from pyspark.sql import functions as f
    df = dfc.select(list(dfc.keys())[0]).toDF()
    df_filtered = df.groupby(["year","month"]).agg(f.sum(f.col("sales")).alias('sales_sum'))
    dyf_filtered = DynamicFrame.fromDF(df_filtered, glueContext, "sales")
    return(DynamicFrameCollection({"CustomTransform0": dyf_filtered}, glueContext))

## @params: [JOB_NAME]
args = getResolvedOptions(sys.argv, ['JOB_NAME'])

sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)
## @type: DataSource
## @args: [database = "challenge-db", table_name = "catalog-raw-ventas", transformation_ctx = "DataSource0"]
## @return: DataSource0
## @inputs: []
DataSource0 = glueContext.create_dynamic_frame.from_catalog(database = "challenge-db", table_name = "catalog-raw-ventas", transformation_ctx = "DataSource0")
## @type: SelectFields
## @args: [paths = ["sales", "year", "month"], transformation_ctx = "Transform2"]
## @return: Transform2
## @inputs: [frame = DataSource0]
Transform2 = SelectFields.apply(frame = DataSource0, paths = ["sales", "year", "month"], transformation_ctx = "Transform2")
## @type: CustomCode
## @args: [dynamicFrameConstruction = DynamicFrameCollection({"Transform2": Transform2}, glueContext), className = FilterNegatives, transformation_ctx = "Transform0"]
## @return: Transform0
## @inputs: [dfc = Transform2]
Transform0 = FilterNegatives(glueContext, DynamicFrameCollection({"Transform2": Transform2}, glueContext))
## @type: CustomCode
## @args: [className = MyTransform2, transformation_ctx = "Transform1"]
## @return: Transform1
## @inputs: [dfc = Transform0]
Transform1 = MyTransform2(glueContext, Transform0)
## @type: SelectFromCollection
## @args: [key = list(Transform1.keys())[0], transformation_ctx = "Transform3"]
## @return: Transform3
## @inputs: [dfc = Transform1]
Transform3 = SelectFromCollection.apply(dfc = Transform1, key = list(Transform1.keys())[0], transformation_ctx = "Transform3")
## @type: DataSink
## @args: [connection_type = "s3", catalog_database_name = "challenge-db", format = "json", connection_options = {"path": "s3://aws-bucket-challenge/procesado/tamal-inc/ventas-mensuales-acumuladas/", "partitionKeys": [], "enableUpdateCatalog":true, "updateBehavior":"UPDATE_IN_DATABASE"}, catalog_table_name = "catalog-venta-acumulada-procesado", transformation_ctx = "DataSink0"]
## @return: DataSink0
## @inputs: [frame = Transform3]
DataSink0 = glueContext.getSink(path = "s3://aws-bucket-challenge/procesado/tamal-inc/ventas-mensuales-acumuladas/", connection_type = "s3", updateBehavior = "UPDATE_IN_DATABASE", partitionKeys = [], enableUpdateCatalog = True, transformation_ctx = "DataSink0")
DataSink0.setCatalogInfo(catalogDatabase = "challenge-db",catalogTableName = "catalog-venta-acumulada-procesado")
DataSink0.setFormat("json")
DataSink0.writeFrame(Transform3)

job.commit()