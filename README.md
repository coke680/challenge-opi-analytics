# Challenge OPI Analytics #

El challenge de OPI Analytics consiste en generar un catálogo de datos a partir de la información de "Tamal-INC" y de "Inventada SA" (ETL's). En segundo lugar, se pide crear un modelo que será evaluado a través de una API. Por último, se pide crear una arquitectura para el ciclo de vida de los modelos.

Dentro del repositorio se adjunta además un archivo con las respuestas a las preguntas teóricas del desafío. El nombre del archivo es "Respuestas Teoría General.txt"

### Herramientas utilizadas ###

* Python
* AWS Glue, AWS Athena, AWS S3, AWS CloudWatch
* FastAPI para el endpoint
* Draw.io para los diseños de las arquitecturas

### 1 - Ingesta de datos ###

* Los datos se encontraban en s3://tamales-inc, por lo cual se hizo una copia dentro de un S3 propio usando el siguiente comando:
```
aws s3 sync s3://tamales-inc s3://bucket-destino
```
* Luego de tener la información guardada en S3, se creó un crawler con Glue para crear un catálogo de datos inicial. Este catálogo mantendrá la primera versión de nuestros datos.
* Se crearon varios Jobs en AWS Glue para implementar los ETL's, los cuales buscan la información de "Tamal-inc" y de "inventada-sa" y la distribuyen en particiones en sus buckets determinados. La partition se realizó por año y mes. Por favor, revisar los screenshots de la carpeta para ver cómo se conformaron los directorios.
* En cada ETL se impiaron los datos (por ejemplo, eliminar venta con números negativos, limpiar nulos, llenar datos faltantes) y se cambiaron los nombres a las columnas tal como aparecía en el archivo "Esquemas.docx".
* Además, junto con cada ETL se creó un catálogo de datos en cada iteración. Con esto nos aseguramos de mantener un lineaje de datos, conservando la historia y las versiones antiguas de la información.
* La información acumulada por mes y las ventas mensuales también quedaron en un catálogo y además, se guardaron en S3. Los screenshots muestran la estructura del datalake.
* La información guardada y los catálogos pueden ser consultados a través de AWS Athena. Se muestran los resultados de una consulta en un screenshot.
* Se adjunta la arquitectura hecha para la ingesta de datos: "Arquitectura Ingesta de Datos.png"
* Se adjunta el modelo de datos de Inventada SA: "Modelo de datos - Inventada SA.png"
* Los archivos "glue-job-raw-to-stage.py" y "glue-job.py" contienen 2 de los ETL's creados. Todos estos son muy similares, sólo hay cambnios en las rutas de S3 y de los catálogos, pero es básicamente lo mismo en todos. Obviamente los podemos revisar en mi cuenta de AWS.
* Me faltó integrar la métrica "Diferencia % vs el mes anterior". Me faltó un poco de tiempo :(
* Sin duda faltó un análisis exploratorio (EDA) a la información y un Feature Engineering. De esta manera podremos encontrar errores, singularidades y se podrá entender de mejor manera cada uno de los datos.

### 2 - Levantar API con FastAPI ###

* Se decidió usar FastAPI para el endpoint del modelo. Favor, instalar FastAPI y Uvicorn (con pip). Con estas herramientas se podrá levantar el servidor y probar los endpoint.
* Para levantar el servidor es necesario tener un archivo "main.py" y utilizar el siguiente comando dentro del mismo directorio.
```
uvicorn main:app --reload
```
* El archivo "main.py" contiene toda la lógica de la aplicación.
* Luego de tener toda la información en orden, con sus correctas partitions y catálogos, se crea una función en Python para poder consultar la información y guardarla en un DataFrame.
* Se crean algunas funciones que servirán para procesar y manipular los datos obtenidos de S3 y también para modelar la función solicitada.
* Se crean 5 URL's para interacturar con el sistema: 
	- GET /
	- GET /get-data
	- GET /training-model
	- GET /predict
	- POST /predict

* GET / es la raíz del proyecto. Se pueden presionar algunos botones que activarán las distintas funciones.
* GET /get-data llama a la función que extrae la información de los catálogos que existen en AWS. Luego de esto, guarda la información en un pickle.
* GET /training-model evaluar la información extraída en el paso anterior sobre la función creada.
* GET /predict es una pequeña prueba de evaluación del modelo. Se utilizan datos dummies.
* POST /predict es el endpoint para evaluar un JSON con datos de entrada.
* Para poder evaluar la función debemos hacerlo a través de docs en FastAPI: [http://localhost:8000/docs#/default/predict_predict_post](http://localhost:8000/docs#/default/predict_predict_post)
* Se adjunta un screenshot con los resultados finales: Un JSON con el resultado de la evaluación.
* Esta API pudo ser levantada a través de contenedores (Docker - KS8), pero se prefirió dejar así para poder clonarla y evaluarla rápidamente.
* Existe una carpeta llamada "model" que es la encargará de alojar el modelo ya entrenado para su posterior uso.

### 3 - Diseño ###

El caso de uso requiere que el equipo recalibre el modelo de machine learning, pero sólo en caso de ser necesario. Esto quiere decir que se re-calibra el modelo solo si el modelo deja de predecir adecuadamente. En el momento en que se detecta que tenemos que re-calibrar el modelo, tenemos 48 horas para hacer ajustes y actualizarlo. Nuestro modelo lleva más de 1 año funcionando.

El equipo necesita una propuesta de pasos y tecnología para el caso descrito anteriormente.

* Existen algunas formas de detectar que el modelo está prediciendo mal:
	- Las métricas de error aumentan.
	- El modelo no se ha reentrenado con nueva información, por lo que comienza a queda obsoleto.
	- A través de los logs.
	- El conocimiento del negocio permite encontrar errores en las predicciones, ya que no se adecuan con la realidad.
* Se crearon 2 diagramas: Uno de flujo de información y otro diagrama de la solución al problema. Se adjuntan los diagramas en la carpeta "diseño".

#### SLA's: ####

* diagrama de la solución al problema:

	- Para el caso del diagrama de la solución al problema, es necesario contar al menos con 5 personas para armar el flujo completo: un DevOps, un Data Engineer, un Arquitecto y un Data Scientist, más un Product Owner del lado del cliente. Si no se puede contar con todos los perfiles, la otra opción podría ser un DevOps, un Machine Learning Engineer y un Product Owner.
	- Los tiempos necesarios para implementar este trabajo (48 hrs en total):
		- 2 horas para averiguar desde donde vino el problema. ¿Datos? ¿parámetros? la revisión de logs es importante.
		- 6 horas para conocer el origen de los datos (sólo set de entrenamiento, no el origen completo), repositorio del modelo, conocer la arquitectura, cómo funciona el modelo y cuál es el problema a trabajar.
		- 8 horas para solucionar el problema en el modelo (Crear nuevo ETL, integrar información al dataset final), reentrenar el modelo y subirlo al repositorio.
		- 4 horas para el CICD, pruebas unitarias, etc.
		- 6 horas para el release y pruebas en producción.
		- Una vez solucionado el problema, dejar al menos 120 horas para automatizar este proceso completo.

* Los Pros de la solución:
	- Escalable
	- Versionable
	- Se apoya en las buenas prácticas de Software Engineering.
	- Uso de herramientas desacopladas. Esto permite mayor control sobre cada uno de los pasos.

* Los Contra de la solución:
	- Conocer el problema y el significado de las variables es fundamental.
	- Dificil de implementar (from scratch) debido a la complejidad de la arquitectura inicial.
	- Se requieren de varios perfiles profesionales para una implementación robusta.
	- Se deja afuera todo proceso de Análisis de datos (EDA).
	- Se deja afuera la Ingenieria de características.


### Contacto ###

* Jorge Luis Martinez - Data & Machine Learning Engineer
* jorge.martinez21@live.cl