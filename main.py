from typing import Optional
from fastapi import FastAPI
import pandas as pd
import numpy as np
import math
import random
import boto3
import s3fs
import time
import pickle

from pydantic import BaseModel
from fastapi import FastAPI, Request
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates


client = boto3.client('glue')
app = FastAPI()
templates = Jinja2Templates(directory="templates")

#función para conectarse a los datos en S3 y extraer el catálogo con los datos.
def get_data():
    GLUE_DATABASE = 'challenge-db'
    ATHENA_S3_OUTPUT_PATH = 's3://aws-bucket-challenge/query-results'

    athena_query = '''SELECT * FROM "catalog-venta-mensual-procesado"'''

    # ejecución de la consulta
    athena_client = boto3.client('athena')
    response = athena_client.start_query_execution(
        QueryString=athena_query,
        QueryExecutionContext={
            'Database': GLUE_DATABASE
        },
        ResultConfiguration={
            'OutputLocation': ATHENA_S3_OUTPUT_PATH
        }
    )

    # rescatar la ejecución de la query
    query_execution_id = response['QueryExecutionId']
    print('Execution ID: ' + query_execution_id)

    # status
    query_execution_status = None
    RETRY_COUNT = 150
    query_status_check_count = 1

    while query_execution_status == 'QUEUED' or query_execution_status == 'RUNNING' or query_execution_status is None:
        # rescatar la ejecución de la query
        query_status = athena_client.get_query_execution(
            QueryExecutionId=query_execution_id)
        query_execution_status = query_status['QueryExecution']['Status']['State']
        print('Status: ' + query_execution_status)
        if query_execution_status == 'FAILED' or query_execution_status == 'CANCELLED':
            raise Exception('Athena query "{}" failed or was cancelled'.format(athena_query))

        if query_status_check_count <= RETRY_COUNT:
            query_status_check_count = query_status_check_count+1

            # añadir 5 segundos para verificar el status de la query
            time.sleep(5)
        else:
            athena_client.stop_query_execution(QueryExecutionId = query_execution_id)
            raise Exception('Time execeeded.')

    print('Consulta "{}" finalizada.'.format(athena_query))

    time.sleep(3)

    # guardar los resultados de la consulta en un csv
    athena_results_filepath = ATHENA_S3_OUTPUT_PATH + '/' + query_execution_id + '.csv'
    print('Resultado: '+ athena_results_filepath)

    # leer el csv anterior y conseguir los datos
    s3FileSystem = s3fs.S3FileSystem(anon=False)
    with s3FileSystem.open(athena_results_filepath, 'rb') as athena_results_file:
        athena_results_df = pd.read_csv(athena_results_file)

    # crear nueva variable de peso por categoria
    athena_results_df['peso_por_categoria_calorica'] = np.where(athena_results_df['calorie_category'] == 'Regular', 1.5, 0.5)

    return athena_results_df

# función para procesar los datos previo a ingresar al modelo. 
# La idea es crear un pequeño pipeline para la limpieza y manipulación del dato. En este caso, solo queremos 2 variables.
def processing_data(dataset):
    dataset = dataset[['sales','peso_por_categoria_calorica']]
    return dataset

# modelo custom creado a partir de las instrucciones en el desafío.
def model_to_evaluate(data):
    return np.log(data['sales']) * random.uniform(-1, 1) + data['peso_por_categoria_calorica']

# root de la API. Devuelve varias opciones con una pequeña interfaz.
@app.get("/", response_class=HTMLResponse)
def read_root(request: Request):
	return templates.TemplateResponse("item.html", {"request": request})    

@app.get("/get-data")
def data():

    # llamar a la función para consultar y guardar los datos
    data = get_data()
    data.to_pickle('dataset/dataset.pkl')

    return {'tabla leída y cargada en un dataframe'}

# URL para entrenar el modelo. En realidad, este modelo no se entrena, sólo se evalua.
@app.get("/training-model")
def training():

    # cargar dataset
    dataset = pd.read_pickle('dataset/dataset.pkl')
    data = processing_data(dataset)

    # evaluar el modelo con los datos de entrada
    model = model_to_evaluate(data)

    ############ proceso normal de entrenamiento y guardado de un modelo ######################

    #from pandas import read_csv
    #from sklearn.model_selection import train_test_split
    #from sklearn.ensemble import RandomForestClassifier
    #from sklearn.metrics import accuracy_score

    # cargar dataset
    ####url = 'https://raw.githubusercontent.com/jbrownlee/Datasets/master/sonar.csv'
    ####dataframe = read_csv(url, header=None)
    ####data = dataframe.values

    # split de inputs
    #X = data.drop(['sales'], axis=1)
    #y = data['sales']

    # split de train y test
    #X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30)

    # Random Forest como modelo
    ####model = RandomForestClassifier(random_state=1)
    ####model.fit(X_train, y_train)

    # evaluar predicciones
    ####evaluate = X_test[0].reshape(1, -1)

    ####yhat = model.predict(evaluate)
    ####result = model.score(X_test, y_test)

    # guardar modelo
    ####filename = 'model/finalized_model.sav'
    ####pickle.dump(model, open(filename, 'wb'))

    return {'Modelo entrenado y guardado. Evaluación: {}'.format(model)}

@app.get("/predict")
def predict():
    # normalmente aquí se hubiera cargado el modelo
    #filename = 'model/finalized_model.sav'
    #model = pickle.load(open(filename, 'rb'))

    # inventar datos para la evaluación del modelo
    evaluate = [[12.0442, 1.5]]
    # guardar los datos anteriores en un dataframe para poder evaluarlo
    df_evaluate = pd.DataFrame(evaluate, columns=['sales','peso_por_categoria_calorica'])

    # crear evaluación
    model = model_to_evaluate(df_evaluate)

    return 'Predicción: {}'.format(model)

# modelo para el request del endpoint
class request_body(BaseModel):
    sales : float
    category_weight : float

# endpoint para predecir valores
@app.post('/predict')
def predict(request : request_body):
    data_endpoint = [[
            request.sales, 
            request.category_weight
    ]]

    # normalmente aquí se hubiera cargado el modelo
    #filename = 'model/finalized_model.sav'
    #model = pickle.load(open(filename, 'rb'))

    # se guardan los datos del endpoint en un dataframe
    df_data_endpoint = pd.DataFrame(data_endpoint, columns=['sales','peso_por_categoria_calorica'])

    # evaluar los datos ingresados en la función
    evaluate = model_to_evaluate(df_data_endpoint)

    return { 'Resultado:' : evaluate }




